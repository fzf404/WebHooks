package config

import (
	"io/ioutil"
	"log"

	"github.com/go-playground/validator/v10"
	"gopkg.in/yaml.v2"
)

// Config 结构
type Config struct {
	Host string `yaml:"host" validate:"required,hostname_port"`

	Hook Hook `yaml:"hook" validate:"required,dive"`

	Mail Mail `yaml:"mail" validate:"omitempty"`
}

// Hooks 结构
type Hook map[string]struct {
	Url    string `yaml:"url" validate:"omitempty,uri"`
	Secret string `yaml:"secret" validate:"omitempty"`
	Shell  string `yaml:"shell" validate:"omitempty,file"`
}

// Mail 结构
type Mail struct {
	Enable bool `yaml:"enable" validate:"omitempty"`

	Host     string `yaml:"host" validate:"required_if=Enable true,omitempty,hostname"`
	Port     int    `yaml:"port" validate:"required_if=Enable true,omitempty,min=1,max=65535"`
	Password string `yaml:"password" validate:"required_if=Enable true,omitempty"`

	From string   `yaml:"from" validate:"required_if=Enable true,omitempty,email"`
	To   []string `yaml:"to" validate:"required_if=Enable true,omitempty,dive,email"`
}

// Config 全局配置
var Cfg Config

// InitConfig 初始化配置
func InitConfig() {

	// 打开配置文件
	file, err := ioutil.ReadFile("./config/config.yaml")
	if err != nil {
		log.Fatal("配置文件打开失败：\n", err.Error())
	}

	// 解析配置文件
	if err := yaml.Unmarshal(file, &Cfg); err != nil {
		log.Fatal("配置文件解析失败：\n", err.Error())
	}

	// 补全配置文件
	for k, v := range Cfg.Hook {
		if v.Url == "" { // 补全 url
			if entry, ok := Cfg.Hook[k]; ok {
				entry.Url = "/" + k
				Cfg.Hook[k] = entry
			}
		}
		if v.Secret == "" { // 补全 secret
			if entry, ok := Cfg.Hook[k]; ok {
				entry.Secret = k
				Cfg.Hook[k] = entry
			}
		}
		if v.Shell == "" { // 补全 shell
			if entry, ok := Cfg.Hook[k]; ok {
				entry.Shell = "./script/" + k + ".sh"
				Cfg.Hook[k] = entry
			}
		}
	}

	// 验证配置文件
	if err := validator.New().Struct(Cfg); err != nil {
		log.Fatal("配置文件验证失败：\n", err.Error())
	}

}
