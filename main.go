package main

import (
	"WebHooks/config"
	"WebHooks/log"
	"WebHooks/pkg/mail"
	"WebHooks/server"
)

func init() {
	log.InitLog()               // 初始化日志
	config.InitConfig()         // 初始化配置
	if config.Cfg.Mail.Enable { // 初始化邮件
		mail.InitMail(config.Cfg.Mail.Host, config.Cfg.Mail.Port, config.Cfg.Mail.Password, config.Cfg.Mail.From, config.Cfg.Mail.To)
	}
	server.InitHook() // 初始化钩子
}

func main() {
	server.InitServer() // 启动服务
}
