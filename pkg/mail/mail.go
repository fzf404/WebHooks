package mail

import (
	"crypto/tls"

	"gopkg.in/gomail.v2"
)

var d *gomail.Dialer
var send []string

// 初始化邮件
func InitMail(host string, port int, password string, from string, to []string) {
	d = gomail.NewDialer(host, port, from, password)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	send = to

}

// 发送邮件
func SendMail(subject string, body string) error {
	m := gomail.NewMessage()

	m.SetHeader("From", d.Username)
	m.SetHeader("To", send...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	return d.DialAndSend(m)
}
