package log

import (
	"io"
	"log"
	"os"
)

// 初始化日志
func InitLog() {

	// 初始化日志信息
	log.SetPrefix("[WebHooks] ")        // 设置日志前缀
	log.SetFlags(log.Ldate | log.Ltime) // 设置日志格式

	// 打开日志文件
	file, err := os.OpenFile("./log/webhooks.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal("日志初始化失败：\n", err)
	}

	// 设置日志输出
	log.SetOutput(io.MultiWriter(file, os.Stdout))
}
