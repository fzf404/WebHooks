package server

import (
	"WebHooks/config"
	"log"
	"net/http"
)

func InitHook() {
	for _, v := range config.Cfg.Hook {
		http.HandleFunc(v.Url, func(w http.ResponseWriter, r *http.Request) {
			log.Println("收到请求：", r.URL.Path)
		})
	}
}

func InitServer() {
	log.Print("服务已启动：",config.Cfg.Host)
	err := http.ListenAndServe(config.Cfg.Host, nil)
	if err != nil {
		log.Fatal("服务启动失败：\n", err)
	}

}
